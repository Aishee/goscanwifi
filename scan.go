package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/smtp"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	listOutput = "listOutput.txt"
)

const (
	smtpServerIP   = ""
	smtpServerPort = ""
)

func main() {
	emailFrom := ""
	emailTo := ""

	wifiIface := "wlan0"
	whiteListSSIDFileName := "bssid.json"

	file := scanWifi(wifiIface)
	fmt.Println(file)

	nodup := CleanDups(file)
	fmt.Println(nodup)

	whiteListWifi := GetSettings(whiteListSSIDFileName)
	fmt.Println(whiteListWifi)

	listScan := []string{}

	for _, ascan := range nodup {
		echeck := isValueInList(ascan, whiteListWifi)
		if echeck != true {
			fmt.Println(ascan, echeck)
			listScan = append(listScan, ascan)
		}
	}
	//Send email
	if emailStatus := sendEmail(emailFrom, emailTo, strings.Join(listScan, "\n")); emailStatus != true {
		fmt.Println("Email could not be sent!")
	}
}

func scanWifi(netiface string) string {
	timestamp := string(time.Now().Format("20060102_150405"))
	wifiFilename := netiface + "_" + timestamp + ".out"

	//run linux command to list wifi networks available
	iwCmd, err := exec.Command("iwlist", netiface, "scanning").Output()
	if err != nil {
		log.Print("Error when getting the interface information.")
	}
	//Write file with the output of the command iwlist to manipulate later
	err = ioutil.WriteFile(wifiFilename, iwCmd, 0644)
	if err != nil {
		log.Print("Error when writing the file. Check user rights: ")
	}
	return wifiFilename
}

func CleanDups(filename string) []string {
	var lineContent string
	encountered := map[string]bool{}

	iwListFile, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer iwListFile.Close()

	scanner := bufio.NewScanner(iwListFile)
	for scanner.Scan() {
		lineContent = scanner.Text()
		if strings.Contains(lineContent, "ESSID") && !strings.Contains(lineContent, "x00") && !strings.Contains(lineContent, "ESSID:\"\"") {
			fatia := []byte(lineContent) // convert the line to byte
			newfatia := []byte{}         //create an empty slice to reconstruct the name of the wifi network
			x := 0
			for i := 27; i < len(fatia); i++ {
				if int(fatia[i]) != 34 {
					newfatia = append(newfatia, fatia[i])
					x++
				} else {
					break
				}
			}
			encountered[string(newfatia)] = true
		}
	}
	nonDupList := []string{}
	for key, _ := range encountered {
		nonDupList = append(nonDupList, key)
	}
	return nonDupList
}

func GetSettings(filename string) []string {
	var lineContentWhiteList string
	sliceWhiteListContent := []string{}
	t := 0
	whitelistFile, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer whitelistFile.Close()
	scannerWhiteList := bufio.NewScanner(whitelistFile)

	for scannerWhiteList.Scan() {
		lineContentWhiteList = scannerWhiteList.Text()
		sliceWhiteListContent = append(sliceWhiteListContent, lineContentWhiteList)
		t++
	}
	return sliceWhiteListContent
}

func isValueInList(scan string, white []string) bool {
	for _, guifi := range white {
		if guifi == scan {
			return true
		}
	}
	return false
}

func sendEmail(from string, to string, txt string) bool {
	//connect to the SMTP server
	connectServer, err := smtp.Dial(smtpServerIP + ":" + smtpServerPort)
	if err != nil {
		log.Fatal(err)
		return false
	}
	//set the sender and recipient
	if err := connectServer.Mail(from); err != nil {
		log.Fatal(err)
		return false
	}

	if err := connectServer.Rcpt(to); err != nil {
		log.Fatal(err)
		return false
	}

	//Email body
	wc, err := connectServer.Data()
	if err != nil {
		log.Fatal(err)
		return false
	}

	txtbody := "PCI COMPLIANCE - AUTOMATED WIRELESS SCAN ROUTINE \nThese are the scan wireless networks detected: \n\n" + txt + "\n\nTimestamp: " + string(time.Now().Format("20060102"))

	_, err = fmt.Fprintf(wc, txtbody)
	if err != nil {
		log.Fatal(err)
		return false
	}

	//Send email
	err = connectServer.Quit()
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}
