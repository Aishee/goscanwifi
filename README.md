# README #

Script wireless scanner is a free tool that allows users to test for the presence of wireless networks and send email alert.
### Features ###

* Scan Wireless
* Compare the output with "whitelisted" networks
* Send an email with unauthorized networks

### Requirements ###

* GNU/Linux
* Go configuration
* apt-get install iwlist

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Install ###

* Clone repository bitbucket 
* Configure email

### Contact ###
* Mail: aishee@aishee.net

### Testing ###
![Testing]
(https://s2.anh.im/2016/08/15/Screenshot-15082016-1144284b35e.png)